/**************************************************************************
   Based on the documentation for the getTemperature and getHumidity functions of the Qwiic Humidity
   library. Additional functionality has been added to enable connections to external APIs via webhooks
   in the Particle Developer Console, monitor battery charge state, and optimize battery life.
    
   Credit to the original makers of the AHT20 library below:
   
   Priyanka Makin @ SparkFun Electronics
   Original Creation Date: March 31, 2020

   SparkFun labored with love to create this code. Feel like supporting open
   source hardware? Buy a board from SparkFun! https://www.sparkfun.com/products/16618

   This code is lemonadeware; if you see me (or any other SparkFun employee)
   at the local, and you've found our code helpful, please buy us a round!


   Distributed as-is; no warranty is given.
 **************************************************************************/
#include <application.h>
#include <Wire.h>
#include "SparkFunMAX17043.h" // Include the SparkFun MAX17043 library
#include <SparkFun_Qwiic_Humidity_AHT20.h> //Click here to get the library: http://librarymanager/All#Qwiic_Humidity_AHT20 by SparkFun
AHT20 humiditySensor;

double voltage = 0; // Variable to keep track of LiPo voltage
double soc = 0; // Variable to keep track of LiPo state-of-charge (SOC)

double humidity = 0.0, fTemp = 0.0;

int led = D7;  // The on-board LED
void setup()
{
    Particle.variable("humidity", &humidity, DOUBLE);
    Particle.variable("fTemp", &fTemp, DOUBLE);
    
  Serial.begin(115200);
  RGB.control(true);
  RGB.color(10, 0, 0);

  Wire.begin(); //Join I2C bus

  //Check if the AHT20 will acknowledge
  if (humiditySensor.begin() == false)
  {
    Particle.publish("AHT20 not detected. Please check wiring. Freezing.");
    while (1);
  }
  Particle.publish("AHT20 acknowledged.");
}

void loop()
{
  //If a new measurement is available
  if (humiditySensor.available() == true)
  {
    Particle.publish("Reading AHT20 data...");
    //Get the new temperature and humidity value
    float temperature = humiditySensor.getTemperature();
    humidity = humiditySensor.getHumidity();
    fTemp = temperature * 1.8 + 32;
    
    // lipo.getVoltage() returns a voltage value (e.g. 3.93)
	voltage = lipo.getVoltage();
	// lipo.getSOC() returns the estimated state of charge (e.g. 79%)
	soc = lipo.getSOC();

    
    Particle.publish("Temperature (°f) : ", String(fTemp));
    delay(500);
    Particle.publish("% Humidity : ", String(humidity));
    delay(500);  
    Particle.publish("SOC %: ", String(soc));
    delay(500);  
    Particle.publish("Voltage: ", String(voltage));
    delay(500);  
    
    //Formats a data sting to pass multiple variables through the "temp" webhook
    String data = String::format("{ \"temp\": %f, \"soc\": %f , \"humidity\": %f }", fTemp, soc, humidity);
    Particle.publish("temp", data, PRIVATE);
    
    
  //The AHT20 can respond with a reading every ~50ms. However, increased read time can cause the IC to heat around 1.0C above ambient.
  //The datasheet recommends reading every 2 seconds.
  delay(300000);
  }


  
  //TODO - fix deep sleep breaking data reading
  //System.sleep(SLEEP_MODE_DEEP, 200);
}
